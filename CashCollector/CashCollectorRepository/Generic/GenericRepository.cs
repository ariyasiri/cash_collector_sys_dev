﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashCollectorRepository.Generic
{
    public class GenericRepository<T, R> : IGenericRepository<T, R> where T : class where R : struct
    {
        //protected readonly DbContext _context;

        //protected readonly DbSet<T> _dbSet;

        //public GenericRepository(DbContext context, DbSet<T> dbSet)
        //{
        //    _context = context;
        //    _dbSet = dbSet;
        //}

        //public int AddSingle(T entity)
        //{
        //    _context.Add(entity);

        //    var noOfRowsAdded = _context.SaveChanges();

        //    if (noOfRowsAdded == 0)
        //    {
        //        throw new DBRecordInsertionException(noOfRowsAdded, 1);
        //    }

        //    return noOfRowsAdded;
        //}

        //public async Task<int> AddSingleAsync(T entity)
        //{
        //    await _context.AddAsync(entity);

        //    var noOfRowsAddedTask = await _context.SaveChangesAsync();

        //    if (noOfRowsAddedTask == 0)
        //    {
        //        throw new DBRecordInsertionException(noOfRowsAddedTask, 1);
        //    }

        //    return noOfRowsAddedTask;
        //}

        //public int AddBulk(IEnumerable<T> entityList)
        //{
        //    _context.AddRange(entityList);

        //    var noOfRowsAdded = _context.SaveChanges();

        //    if (noOfRowsAdded != entityList.Count())
        //    {
        //        throw new DBRecordInsertionException(noOfRowsAdded, entityList.Count());
        //    }

        //    return noOfRowsAdded;
        //}

        //public async Task<int> AddBulkAsync(IEnumerable<T> entityList)
        //{
        //    _context.AddRange(entityList);
        //    var noOfRowsAdded = await _context.SaveChangesAsync();

        //    if (noOfRowsAdded != entityList.Count())
        //    {
        //        throw new DBRecordInsertionException(noOfRowsAdded, entityList.Count());
        //    }

        //    return noOfRowsAdded;
        //}

        //public int EditSingle(T entity)
        //{
        //    var noOfRowsAdded = _context.SaveChanges();
        //    return noOfRowsAdded;
        //}

        //public async Task<int> EditSingleAsync(T entity)
        //{
        //    var noOfRowsAdded = await _context.SaveChangesAsync();
        //    return noOfRowsAdded;
        //}

        //public int EditSingle(T entity, int key)
        //{
        //    T existing = _context.Set<T>().Find(key);
        //    var noOfRowsAdded = 0;

        //    if (existing != null)
        //    {
        //        _context.Entry(existing).CurrentValues.SetValues(entity);
        //        noOfRowsAdded = _context.SaveChanges();
        //    }

        //    return noOfRowsAdded;
        //}

        //public async Task<int> EditSingleAsync(T entity, int key)
        //{
        //    T existing = await _context.Set<T>().FindAsync(key);
        //    var noOfRowsAdded = 0;

        //    if (existing != null)
        //    {
        //        _context.Entry(existing).CurrentValues.SetValues(entity);
        //        noOfRowsAdded = await _context.SaveChangesAsync();
        //    }

        //    return noOfRowsAdded;
        //}

        //public int EditBulk(IList<T> entityList)
        //{
        //    var noOfRowsAdded = _context.SaveChanges();
        //    return noOfRowsAdded;
        //}

        //public async Task<int> EditBulkAsync(IList<T> entityList)
        //{
        //    var noOfRowsAdded = await _context.SaveChangesAsync();
        //    return noOfRowsAdded;
        //}

        //public int DeleteSingle(R id)
        //{
        //    T entity = this.FindById(id);
        //    if (entity == null)
        //    {
        //        throw new InvalidOperationException("Unable to delete a null object");
        //    }
        //    _context.Remove(entity);
        //    var noOfRowsAdded = _context.SaveChanges();
        //    return noOfRowsAdded;
        //}

        //public int Delete(T entity)
        //{
        //    _dbSet.Remove(entity);
        //    var noOfRowsDeleted = _context.SaveChanges();
        //    return noOfRowsDeleted;
        //}

        //public async Task<int> DeleteAsync(T entity)
        //{
        //    _dbSet.Remove(entity);
        //    var noOfRowsDeleted = await _context.SaveChangesAsync();
        //    return noOfRowsDeleted;
        //}

        //public T FindById(R id)
        //{
        //    T foundEntity = _dbSet.Find(id);
        //    return foundEntity;
        //}

        //public async Task<T> FindByIdAsync(R id)
        //{
        //    T foundEntity = await _dbSet.FindAsync(id);
        //    return foundEntity;
        //}

        //protected IEnumerable<T> FindList(Func<T, bool> predicate)
        //{
        //    IEnumerable<T> recordSet = _dbSet.Where(predicate);
        //    return recordSet;
        //}

        //protected T FindSingle(Func<T, bool> predicate)
        //{
        //    T entity = _dbSet.SingleOrDefault(predicate);
        //    return entity;
        //}

        //public int DeleteBulk(IList<T> entityList)
        //{
        //    _context.RemoveRange(entityList);
        //    var noOfRowsDeleted = _context.SaveChanges();
        //    return noOfRowsDeleted;
        //}

        //public async Task<int> DeleteBulkAsync(IList<T> entityList)
        //{
        //    _context.RemoveRange(entityList);
        //    var noOfRowsDeleted = await _context.SaveChangesAsync();
        //    return noOfRowsDeleted;
        //}

        //public async Task<int> DeleteSingleAsync(R id)
        //{
        //    T entity = await FindByIdAsync(id);
        //    if (entity == null)
        //    {
        //        throw new InvalidOperationException("Unable to delete a null object");
        //    }
        //    _context.Remove(entity);
        //    var noOfRowsAdded = await _context.SaveChangesAsync();
        //    return noOfRowsAdded;
        //}

        //public async Task<IList<T>> GetAllAsync()
        //{
        //    var result = await _dbSet.ToListAsync();
        //    return result;
        //}

        //public IList<T> GetAll()
        //{
        //    var result = _dbSet.ToList();
        //    return result;
        //}
    }
}
