﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CashCollectorRepository.Generic
{
    public interface IGenericRepository<T, R> where T : class where R : struct
    {
        //int AddSingle(T entity);

        //Task<int> AddSingleAsync(T entity);

        //int AddBulk(IEnumerable<T> entityList);

        //Task<int> AddBulkAsync(IEnumerable<T> entityList);

        //int EditSingle(T entity);

        //Task<int> EditSingleAsync(T entity);

        //int EditBulk(IList<T> entityList);

        //Task<int> EditBulkAsync(IList<T> entityList);

        //int DeleteSingle(R id);

        //Task<int> DeleteSingleAsync(R id);

        //int Delete(T entity);

        //Task<int> DeleteAsync(T entity);

        //int DeleteBulk(IList<T> entityList);

        //Task<int> DeleteBulkAsync(IList<T> entityList);

        //T FindById(R id);

        //Task<T> FindByIdAsync(R id);

        //int EditSingle(T entity, int key);

        //Task<int> EditSingleAsync(T entity, int key);

        //Task<IList<T>> GetAllAsync();

        //IList<T> GetAll();
    }
}
